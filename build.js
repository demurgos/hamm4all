const commonmark = require("commonmark");
const fs = require("fs");
const {sync: glob} = require("glob");
const sysPath = require("path");
const pug = require("pug");
const toml = require("toml");

const SERVER_URL = "https://hamm4all.eternalfest.net/";
const PROJECT_ROOT = __dirname;
const SRC_DIR = sysPath.join(PROJECT_ROOT, "src");
const HFEST_LEVELS_DIR = sysPath.join(SRC_DIR, "niveaux");
const HFEST_LEVEL_TEMPLATE = sysPath.join(SRC_DIR, "niveaux", "_niveau.pug");
const NIV_LEVEL_TEMPLATE = sysPath.join(SRC_DIR, "eternalfest", "_niveau.pug");
const CREATIONS_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/creations");
const CONTREES_ETERNAL_LEVELS_DIR = sysPath.join(SRC_DIR, "eternalfest/contrees");
const BUILD_DIR = sysPath.join(PROJECT_ROOT, "build");
const MD_PARSER = new commonmark.Parser();
const MD_RENDERER = new commonmark.HtmlRenderer();

function build() {
  copyAssets();
  renderHfestLevels();
  renderPugFiles();
  renderEternalfestLevels(CREATIONS_LEVELS_DIR)
  renderEternalfestLevels(CONTREES_ETERNAL_LEVELS_DIR)
}

function copyAssets() {
  for (filePath of glob("/**/*.{png,svg,css}", {root: SRC_DIR, nodir: true})) {
    const outPath = sysPath.join(BUILD_DIR, sysPath.relative(SRC_DIR, filePath));
    copyFileSync(filePath, outPath);
  }
}

function renderPugFiles() {
  for (pugFile of getPugFilesFromDir(SRC_DIR)) {
    const relPath = sysPath.relative(SRC_DIR, pugFile);
    console.log(`Rendering ${relPath}`);
    const dataPath = pugFile.replace(/\.pug$/, ".json");
    const outPath = sysPath.join(BUILD_DIR, relPath).replace(/\.pug$/, ".html");
    const renderer = pug.compileFile(pugFile);
    const data = fs.existsSync(dataPath) ? require(dataPath) : {};
      data.imageExists = (imagePath) => {
        return fs.existsSync(sysPath.join(SRC_DIR, imagePath));
    }
    const html = renderer(data);
    outputFileSync(outPath, Buffer.from(html));
  }
}

function renderHfestLevels() {
  const renderer = pug.compileFile(HFEST_LEVEL_TEMPLATE);
  for (mdFile of getMdFilesFromDir(HFEST_LEVELS_DIR)) {
    const relPath = sysPath.relative(SRC_DIR, mdFile);
    console.log(`Rendering ${relPath}`);
    const outPath = sysPath.join(BUILD_DIR, relPath).replace(/\.md$/, ".html");
    const levelData = loadMdFile(mdFile);
    levelData.server_url = SERVER_URL;
    const html = renderer(levelData)
    outputFileSync(outPath, Buffer.from(html));
  }
}

function renderEternalfestLevels(land) {
  const renderer = pug.compileFile(NIV_LEVEL_TEMPLATE);
  for (mdFile of getMdFilesFromDir(land)) {
    const relPath = sysPath.relative(SRC_DIR, mdFile);
    console.log(`Rendering ${relPath}`);
    const outPath = sysPath.join(BUILD_DIR, relPath).replace(/\.md$/, ".html");
    const levelData = loadMdFile(mdFile);
    levelData.server_url = SERVER_URL;
    const html = renderer(levelData, {server_url: SERVER_URL});
    outputFileSync(outPath, Buffer.from(html));
  }
}

function getPugFilesFromDir(dirPath) {
  return glob("/**/[!_]*.pug", {root: dirPath, nodir: true})
}

function getMdFilesFromDir(dirPath) {
  return glob("/**/[!_]*.md", {root: dirPath, nodir: true})
}

function loadMdFile(filePath) {
  const sourceText = fs.readFileSync(filePath, {encoding: "UTF-8"});
  const parsed = MD_PARSER.parse(sourceText);
  const data = {};
  if (parsed.firstChild !== null && parsed.firstChild.type === "code_block" && parsed.firstChild.info === "toml") {
    // The Markdown file starts with a TOML block of metadata
    const tomlText = parsed.firstChild.literal;
    parsed.firstChild.unlink();
    Object.assign(data, toml.parse(tomlText));
  }
  data.content = MD_RENDERER.render(parsed);
  return data;
}

function outputFileSync(filePath, data) {
  const dirPath = sysPath.dirname(filePath);
  fs.mkdirSync(dirPath, {recursive: true});
  fs.writeFileSync(filePath, data);
}

function copyFileSync(srcPath, destPath) {
  outputFileSync(destPath, fs.readFileSync(srcPath));
}

build();
