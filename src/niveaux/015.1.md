```toml
title = "Niveau 15.1"
images = ["/niveaux/015.1.png"]
video = "https://www.youtube.com/embed/ZVr9wTl5zxg"

[nav]
prev = "/niveaux/015.0.html"
next = "/niveaux/015.2.html"
```