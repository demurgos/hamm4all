```toml
title = "Niveau 15.11.3"
images = ["/niveaux/015.11.3.png"]
video = "https://www.youtube.com/embed/YNseADdyMxc"

[nav]
prev = "/niveaux/015.11.2.html"
next = "/niveaux/015.11.4.html"
```