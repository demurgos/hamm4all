```toml
title = "Niveau 15.11.8"
images = ["/niveaux/015.11.8.png"]
video = "https://www.youtube.com/embed/bHZprWPR6Gk"

[nav]
prev = "/niveaux/015.11.7.html"
next = "/niveaux/015.11.9.html"
```